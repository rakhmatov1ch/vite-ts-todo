import { useState } from "react"
import Header from "./components/Header.tsx"
import Form from "./components/Form.tsx"
import Todos from "./components/Todos.tsx"
import { ITodo } from "./types"

const initialState: ITodo[] = [
    { id: 1, title: "First todo", completed: false },
    { id: 2, title: "Second todo", completed: true },
    { id: 3, title: "Third todo", completed: false }
]

function App() {
    const [todos, setTodos] = useState<ITodo[]>(initialState)
    const [editing, setEditing] = useState<ITodo | null>(null)

    return (
        <div
            className='flex-1 bg-gradient-to-br from-violet-900 to-violet-500 h-screen flex justify-center items-center'>
            <div className='w-[40%] min-w-[600px] p-4 bg-[#1d1546] rounded-xl'>
                <Header/>
                <Form setTodos={setTodos} editing={editing} setEditing={setEditing}/>
                <Todos todos={todos} setTodos={setTodos} setEditing={setEditing}/>
            </div>
        </div>
    )
}

export default App
