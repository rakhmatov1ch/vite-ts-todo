import React, { useEffect } from "react"
import { FormEvent, useState } from "react"
import { ITodo } from "../types"


interface IFormProps {
    setTodos: React.Dispatch<React.SetStateAction<ITodo[]>>;
    setEditing: React.Dispatch<React.SetStateAction<ITodo | null>>;
    editing: ITodo | null
}

const Form = ({ setTodos, setEditing, editing }: IFormProps) => {
    const [value, setValue] = useState<string>("")

    useEffect(() => {
        if (editing !== null) {
            setValue(editing.title)
        }
    }, [editing])
    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (editing === null) {
            setTodos((prev: ITodo[]) => [...prev, {
                id: Date.now() + Math.random(),
                title: value,
                completed: false
            }])
            setValue("")
            return
        }
        setTodos((prev: ITodo[]) => [...prev.map(item => {
            if (item.id === editing.id) {
                item.title = value
            }
            return item
        })])
        setValue("")
        setEditing(null)
    }

    return (
        <div className='p-5'>
            <form className='flex items-center justify-center border-2 border-[#852df5]' onSubmit={handleSubmit}>
                <input
                    className='border-0 w-[80%] text-lg px-2 focus:outline-none bg-transparent text-purple-50'
                    type="text"
                    placeholder='What would you like to do?'
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                />
                <button className='w-1/5 bg-[#852df5] text-white py-2'>
                    Add Task
                </button>
            </form>
        </div>
    )
}

export default Form
