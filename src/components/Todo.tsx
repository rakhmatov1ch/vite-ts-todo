import { ITodo } from "../types"
import React from "react"


interface ITodoProps {
    todo: ITodo;
    setTodos: React.Dispatch<React.SetStateAction<ITodo[]>>;
    setEditing: React.Dispatch<React.SetStateAction<null | ITodo>>;
}

const Todo = ({ todo, setTodos, setEditing }: ITodoProps) => {

    const deleteTodo = () => {
        const confirmation = confirm("Do you really want to delete this todo?")
        confirmation && setTodos((prev: ITodo[]) => [...prev.filter(prevTodo => prevTodo.id !== todo.id)])
    }

    const handleEdit = () => {
        setEditing(todo)
    }

    const todoComplete = () => {
        setTodos((prev: ITodo[]) => prev.map(item => {
            if (item.id === todo.id) {
                item.completed = !item.completed
            }
            return item
        }))
    }

    return (
        <div className='w-full px-4 py-3 bg-[#852df5] text-white rounded-md mb-3 flex justify-between items-center'>
            <h1 className='text-lg font-semibold max-w-3/5 overflow-x-hidden'>{todo.title}</h1>

            <div className='flex gap-1 items-center'>
                <button
                    onClick={todoComplete}
                    className={`text-center w-24 mx-2 p-1 rounded-xl cursor-pointer font-bold text-sm 
                    ${!todo.completed ? "bg-red-600/90" : "bg-green-500/90"}`}>
                    {!todo.completed ? "open" : "completed"}
                </button>
                <svg onClick={handleEdit} className='w-8 h-8' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M384 224v184a40 40 0 01-40 40H104a40 40 0 01-40-40V168a40 40 0 0140-40h167.48" fill="none"
                          stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="32"/>
                    <path
                        fill='#fff'
                        d="M459.94 53.25a16.06 16.06 0 00-23.22-.56L424.35 65a8 8 0 000 11.31l11.34 11.32a8 8 0 0011.34
                        0l12.06-12c6.1-6.09 6.67-16.01.85-22.38zM399.34 90L218.82 270.2a9 9 0 00-2.31 3.93L208.16
                        299a3.91 3.91 0 004.86 4.86l24.85-8.35a9 9 0 003.93-2.31L422 112.66a9 9 0 000-12.66l-9.95-10a9
                        9 0 00-12.71 0z"/>
                </svg>
                <svg onClick={deleteTodo} xmlns="http://www.w3.org/2000/svg" className='w-8 h-8' viewBox="0 0 512 512">
                    <path d="M448 256c0-106-86-192-192-192S64 150 64 256s86 192 192 192 192-86 192-192z" fill="none"
                          stroke="currentColor" strokeMiterlimit="10" strokeWidth="32"/>
                    <path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"
                          strokeWidth="32" d="M320 320L192 192M192 320l128-128"/>
                </svg>
            </div>
        </div>
    )
}

export default Todo
