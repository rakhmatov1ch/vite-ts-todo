const Header = () => {
    return (
        <div className='p-5'>
            <h1 className='text-4xl font-semibold text-purple-50 text-center'>Get Things Done!</h1>
        </div>
    )
}

export default Header
