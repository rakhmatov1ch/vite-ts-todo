import React from "react"
import Todo from "./Todo.tsx"
import { ITodo } from "../types"

interface ITodosProps {
    todos: ITodo[];
    setTodos: React.Dispatch<React.SetStateAction<ITodo[]>>;
    setEditing: React.Dispatch<React.SetStateAction<null | ITodo>>;
}

const Todos = ({ todos, setTodos, setEditing }: ITodosProps) => {
    return (
        <div className='rounded-2xl p-5 h-[400px] overflow-y-scroll scroll'>
            {todos && todos.map(todo => (
                <Todo todo={todo} setTodos={setTodos} setEditing={setEditing} key={todo.id}/>
            ))}
        </div>
    )
}

export default Todos
